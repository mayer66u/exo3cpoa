package exo3cpoa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Formation {

	private String id;
	private Map<Matiere,Double> mat;
	
	public Formation(String i){
		this.id = i;
		this.mat = new HashMap<Matiere,Double>();
	}
	
	public void ajouterMatiere(Matiere mati, Double coeff){
		this.mat.put(mati, coeff);
	}
	public void supprimerMatiere(Matiere mat) throws FormationException {
		if(this.mat.containsKey(mat)) {
			this.mat.remove(mat);
		} else {
			throw new FormationException("Cette matiere n'est pas présente dans la formation");
		}
	}
	
	public String getIdentifiant() {
		return id;
	}
	
	public Set<Matiere> getMatieres() {
		return this.mat.keySet();
	}
	
	public Map<Matiere,Double> getMatieresCoeff() {
		return this.mat;
	}
	
	public void setCoeff(Matiere mat, Double coeff) throws FormationException {
		if(this.mat.containsKey(mat)) {
			this.mat.replace(mat, coeff);
		} else {
			throw new FormationException("Cette matiere n'est pas présente dans la formation");
		}
	}
	
	public Double getCoeff(Matiere mat) throws FormationException {
		Double res = -1.0;
		if(this.mat.containsKey(mat)) {
			res = this.mat.get(mat);
		} else {
			throw new FormationException("Cette matiere n'est pas présente dans la formation");
		}
		return res;
	}
	
	public boolean contientMatiere(Matiere mat) {
		return this.mat.containsKey(mat);
	}
	
	public void vider() {
		this.mat.clear();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((mat == null) ? 0 : mat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Formation other = (Formation) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (mat == null) {
			if (other.mat != null)
				return false;
		} else if (!mat.equals(other.mat))
			return false;
		return true;
	}
	
	
	
}
