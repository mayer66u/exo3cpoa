package exo3cpoa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Resultat {
	
	private Map<Matiere, List<Double>> matieres;
	
	public Resultat(Formation form) {
		this.matieres = new HashMap<Matiere, List<Double>>();
		for(Matiere mat : form.getMatieres()) {
			this.matieres.put(mat, new ArrayList<Double>());
		}
	}
	
	public Set<Matiere> getMatieres(){
		return this.matieres.keySet();
	}
	
	public Map<Matiere, List<Double>> getMatieresResultats() {
		return matieres;
	}
	
	public List<Double> getResultats(Matiere mat) throws ResultatException{
		List<Double> res = null;
		if(this.matieres.containsKey(mat)) {
			res = this.matieres.get(mat);
		}else {
			throw new ResultatException("Cette mati�re n'existe pas");
		}
		return res;
		
	}
	
	public void addMatiere(Matiere mat) throws ResultatException{
		if(!this.matieres.containsKey(mat)) {
			this.matieres.put(mat, new ArrayList<Double>());
		}else {
			throw new ResultatException("Mati�re d�j� pr�sente");
		}
	}
	
	public void addResultat(Matiere mat, Double note) throws ResultatException{
		if(this.matieres.containsKey(mat)) {
			if((note >= 0.0) && (note <=20.0)) {
				List<Double> l = this.getResultats(mat);
				l.add(note);
				this.matieres.replace(mat, l);
			} else {
				throw new ResultatException("La note doit �tre comprise entre 0 et 20");
			}
		}else {
			throw new ResultatException("Cette mati�re n'existe pas");
		}
	}
	
	public void viderMatiere(Matiere mat) throws ResultatException{
		if(this.matieres.containsKey(mat)) {
			this.matieres.replace(mat, new ArrayList<Double>());
		}else {
			throw new ResultatException("Mati�re non pr�sente");
		}
	}
	
	public void vider() {
		this.matieres.clear();
	}

	
}
