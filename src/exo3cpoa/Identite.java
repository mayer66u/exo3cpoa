package exo3cpoa;

public class Identite {
	
	private String nip;
	private String nom;
	private String prenom;
	
	/**
	 * Constructeur
	 * @param num numero d'Identification Personnel
	 * @param n nom
	 * @param p prenom
	 */
	
	public Identite(String num, String n, String p){
		this.nip = num;
		this.nom = n;
		this.prenom = p;
	}

	public String getNip() {
		return nip;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	@Override
	public String toString() {
		return nom;
	}
	

}
