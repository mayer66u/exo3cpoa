package exo3cpoa;

@SuppressWarnings("serial")
public class FormationException extends Exception {
	
	public FormationException(String string) {
		super(string);
	}

}

