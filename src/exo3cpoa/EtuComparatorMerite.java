package exo3cpoa;

import java.util.Comparator;

public class EtuComparatorMerite implements Comparator<Etudiant>{

	@Override
	public int compare(Etudiant o1, Etudiant o2) {
		int res = 0;
		try {
			if(o1.calculerMoyenneGenerale() < o2.calculerMoyenneGenerale()) {
				res = 1;
			} else {
				res = -1;
			}
		}catch (ResultatException e) {
			res = 0;
		}
		return res;
	}
	

}
