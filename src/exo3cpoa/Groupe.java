package exo3cpoa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Groupe {
	
	private Formation formation;
	private List<Etudiant> etudiants;
	
	public Groupe(Formation f) {
		this.formation = f;
		this.etudiants = new ArrayList<Etudiant>();
	}

	/**
	 * @return the formation
	 */
	public Formation getFormation() {
		return formation;
	}

	/**
	 * @return the etudiants
	 */
	public List<Etudiant> getEtudiants() {
		return etudiants;
	}
	
	public void ajouterEtudiant(Etudiant e) throws GroupeException {
		if(!this.etudiants.contains(e)) {
			if(this.formation.equals(e.getFormation())) {
				this.etudiants.add(e);
			} else {
				throw new GroupeException("L'�tudiant n'a pas la m�me formation que le groupe");
			}
		} else {
			throw new GroupeException("L'�tudiant fait d�j� parti du groupe");
		}
	}
	
	public void supprimerGroupe(Etudiant e) throws GroupeException{
		if(this.etudiants.contains(e)) {
			this.etudiants.remove(e);
		} else {
			throw new GroupeException("L'�tudiant ne fait pas parti du groupe");
		}
	}
	
	public Double calculerMoyenneMatiere(Matiere m) throws ResultatException {
		Double res = 0.0;
		for (Etudiant etu : this.etudiants) {
			res += etu.calculerMoyenneMatiere(m);
		}
		res /= this.etudiants.size();
		return res;
	}
	
	public Double calculerMoyenneGenerale() throws ResultatException {
		Double res = 0.0;
		Map<Matiere, Double> moyennes = new HashMap<Matiere, Double>();
		for(Matiere m : this.formation.getMatieres()) {
			Double moy = 0.0;
			for(Etudiant e : this.etudiants) {
				moy += e.calculerMoyenneMatiere(m);
			}
			moy /= this.etudiants.size();
			moyennes.put(m, moy);
		}
		for(Matiere m : moyennes.keySet()) {
			res += moyennes.get(m);
		}
		res /= moyennes.size();
		return res;
	}
	
	public void trierParMerite(){
		Collections.sort(this.etudiants, new EtuComparatorMerite());
	}
	
	public void trierAlpha() {
		Collections.sort(this.etudiants, new EtuComparatorAlpha());
	}
}
