package exo3cpoa;

import java.util.Comparator;

public class EtuComparatorAlpha implements Comparator<Etudiant>{

	@Override
	public int compare(Etudiant o1, Etudiant o2) {
		return o1.getIdentite().getNom().compareTo(o2.getIdentite().getNom());
	}

}
