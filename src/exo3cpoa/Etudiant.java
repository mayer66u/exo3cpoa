package exo3cpoa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Etudiant {
	
	private Identite identite;
	private Formation formation;
	private Resultat resultats;
	
	public Etudiant(Identite id, Formation f, Resultat res){
		this.identite = id;
		this.formation = f;
		this.resultats = res;
	}

	public Identite getIdentite() {
		return identite;
	}

	public Formation getFormation() {
		return formation;
	}

	public Resultat getResultats() {
		return resultats;
	}
	
	public void ajouterNote(Matiere m, Double n) throws ResultatException {
		this.resultats.addResultat(m, n);
	}
	
	public Double calculerMoyenneMatiere(Matiere m) throws ResultatException {
		Double res = 0.0;
		for (Double n : this.resultats.getResultats(m)) {
			res += n;
		}
		res = res / this.resultats.getResultats(m).size();
		return res;
	}

	public Double calculerMoyenneGenerale() throws ResultatException {
		Double res = 0.0;
		for (Matiere m : this.resultats.getMatieres()) {
			res += calculerMoyenneMatiere(m);
		}
		res = res / this.resultats.getMatieres().size();
		return res;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((identite == null) ? 0 : identite.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return ""+identite;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Etudiant other = (Etudiant) obj;
		if (identite == null) {
			if (other.identite != null)
				return false;
		} else if (!identite.equals(other.identite))
			return false;
		return true;
	}

}
