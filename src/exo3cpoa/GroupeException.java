package exo3cpoa;

public class GroupeException extends Exception{
	
	public GroupeException(String message) {
		super(message);
	}
}
