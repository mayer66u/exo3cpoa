package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import exo3cpoa.Formation;
import exo3cpoa.FormationException;
import exo3cpoa.Matiere;

public class TestFormation {
	
	private Formation formation;
	private Matiere m1, m2;
	
	@Before
	public void setUp() throws Exception {
		this.formation = new Formation("Formation test");
		m1 = new Matiere("Base de donn�es");
		m2 = new Matiere("Maths");
		
	}

	@Test
	public void testFormationAddMatiere() throws FormationException {
		
		this.formation.ajouterMatiere(m1, 3.0);
		assertEquals("Il devrait y avoir 1 matiere", 1, this.formation.getMatieres().size());
		assertEquals("La mati�re devrait avoir un coeeficiant de 3", Double.valueOf(3.0), this.formation.getCoeff(m1));
	}
	
	@Test
	public void testFormationEnleverMatiereOK() throws FormationException {
		this.formation.ajouterMatiere(m1, 3.0);
		this.formation.ajouterMatiere(m2, 2.0);
		
		this.formation.supprimerMatiere(m1);
		
		assertEquals("Il devrait y avoir 1 matiere", 1, this.formation.getMatieres().size());
		assertEquals("La mati�re devrait avoir un coeeficiant de 2", Double.valueOf(2.0), this.formation.getCoeff(m2));
	}
	
	@Test(expected = FormationException.class)
	public void testFormationEnleverMatiereException() throws FormationException {
		this.formation.ajouterMatiere(m1, 3.0);
		this.formation.supprimerMatiere(m2);
	}
	
	@Test
	public void testFormationContientMatiereOK() {
		this.formation.ajouterMatiere(m1, 3.0);
		
		boolean res = this.formation.contientMatiere(m1);
		
		assertTrue("La mati�re doit �tre pr�sente", res);
	}
	
	@Test
	public void testFormationContientPasMatiere() {
		this.formation.ajouterMatiere(m1, 3.0);
		boolean res = this.formation.contientMatiere(m2);
		
		assertFalse("La mati�re ne doit pas �tre pr�sente", res);
	}

}
