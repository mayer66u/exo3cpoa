package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import exo3cpoa.Etudiant;
import exo3cpoa.Formation;
import exo3cpoa.Identite;
import exo3cpoa.Matiere;
import exo3cpoa.Resultat;
import exo3cpoa.ResultatException;

public class TestEtudiant {
	
		private Etudiant e;
		private Matiere cpoa;
		private Matiere maths;
		
		@Before
		public void initialisation() {
			//Jeu de données
			Identite id = new Identite("1234", "Jacques", "Kiki");
			cpoa = new Matiere("CPOA");
			maths = new Matiere("Mathematiques");
			Formation formation = new Formation("DUT Info");
			formation.ajouterMatiere(cpoa, 1.0);
			formation.ajouterMatiere(maths, 1.0);
			Resultat resultats = new Resultat(formation);
			try {
				resultats.addResultat(cpoa, 20.0);
				resultats.addResultat(cpoa, 10.0);
				resultats.addResultat(maths, 10.0);
				resultats.addResultat(maths, 0.0);
			} catch (ResultatException e1) {
				e1.printStackTrace();
			}
			e = new Etudiant(id, formation, resultats);
		}
		
		/**
		 * Test d'insertion d'une note supérieure à 20
		 */
		@Test(expected = ResultatException.class)
		public void testInsertInvalidNote() throws ResultatException {
			//Instruction de test
			e.ajouterNote(new Matiere("Informatique"), 24.0);
		}
		
		/**
		 * Test d'insertion d'une matière non ajoutée aux résultats de l'étudiant
		 */
		@Test(expected = ResultatException.class)
		public void testInsertInvalidMatiere() throws ResultatException {
			//Instruction de test
			e.ajouterNote(new Matiere("Informatique"), 17.0);
		}
		
		/**
		 * Test d'insertion d'une note correcte
		 */
		@Test
		public void testInsertValidNote() throws ResultatException {
			//Méthode à tester
			
			e.ajouterNote(cpoa, 17.0);
			//Verification des résultats
			assertEquals("La matiere 'CPOA' doit avoir la note 17", Double.valueOf(17.0), e.getResultats().getMatieresResultats().get(cpoa).get(2));
		}
		
		/**
		 * Test du calcul de la moyenne d'une matière qui n'a pas été ajoutée aux résultats de l'étudiant
		 */
		@Test(expected = ResultatException.class)
		public void testCalculMoyenneInvalidMatiere() throws ResultatException {
			//Instruction de test
			e.calculerMoyenneMatiere(new Matiere("Anglais"));
		}
		
		/**
		 * Test du calcul de la moyenne d'une matiere correctement ajout�e � l'�tudiant
		 */
		@Test
		public void testCalculMoyenneValidMatiere() throws ResultatException {
			//Instruction de test
			
			assertEquals("La moyenne devrait être de 15.0", Double.valueOf(15.0), e.calculerMoyenneMatiere(cpoa));
		}
		
		/**
		 * Test du calcul de la moyenne général de l'étudiant
		 */
		@Test
		public void testCalculMoyenneValidMatiereGenerale() throws ResultatException {
			//Instruction de test
			assertEquals("La moyenne générale devrait être de 10.0", Double.valueOf(10.0), e.calculerMoyenneGenerale());
		}

}
