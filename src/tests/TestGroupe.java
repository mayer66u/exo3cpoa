package tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import exo3cpoa.Etudiant;
import exo3cpoa.Formation;
import exo3cpoa.Groupe;
import exo3cpoa.GroupeException;
import exo3cpoa.Identite;
import exo3cpoa.Matiere;
import exo3cpoa.Resultat;
import exo3cpoa.ResultatException;



public class TestGroupe {
	
	private Etudiant etu;
	private Groupe grp;
	private Formation formation;
	
	@Before
	public void setUp() {
		this.formation = new Formation("Formation test");
		this.grp = new Groupe(formation);
		
		Identite id = new Identite("1234", "Th�o", "Nico");
		Matiere cpoa = new Matiere("CPOA");
		Matiere maths = new Matiere("Mathematiques");
		formation.ajouterMatiere(cpoa, 1.0);
		formation.ajouterMatiere(maths, 1.0);
		Resultat resultats = new Resultat(formation);
		try {
			resultats.addResultat(cpoa, 20.0);
			resultats.addResultat(cpoa, 10.0);
			resultats.addResultat(maths, 10.0);
			resultats.addResultat(maths, 0.0);
		} catch (ResultatException e1) {
			e1.printStackTrace();
		}
		etu = new Etudiant(id, formation, resultats);
	}
	
	@Test
	public void testAjouterEtudiantPossible() throws GroupeException {
		this.grp.ajouterEtudiant(etu);
		
		assertEquals("Il devrait y avoir un etudiant", this.grp.getEtudiants().get(0), this.etu);
	}
	
	@Test(expected = GroupeException.class)
	public void testAjouterEtudiantImpossibleEtu() throws GroupeException {
		this.grp.ajouterEtudiant(etu);
		this.grp.ajouterEtudiant(etu);
	}
	
	@Test(expected = GroupeException.class)
	public void testAjouterEtudiantImpossibleFormation() throws GroupeException {
		Formation f = new Formation("Echec");
		Identite i = new Identite("5678", "Mayer", "Kircher");
		Resultat res = new Resultat(f);
		Etudiant e = new Etudiant(i, f, res);
		
		this.grp.ajouterEtudiant(this.etu);
		this.grp.ajouterEtudiant(e);
	}
	
	@Test
	public void testSupprimerGroupeOK() throws GroupeException {
		this.grp.ajouterEtudiant(etu);
		this.grp.supprimerGroupe(etu);
		
		assertEquals("L'�tudiant ne devrait plus �tre dans le groupe", 0, this.grp.getEtudiants().size());
	}
	
	@Test(expected = GroupeException.class)
	public void testSupprimerGroupePasOK() throws GroupeException {
		
		Identite i = new Identite("5678", "Mayer", "Kircher");
		Resultat res = new Resultat(this.formation);
		Etudiant e = new Etudiant(i, this.formation, res);
		
		this.grp.ajouterEtudiant(this.etu);
		this.grp.supprimerGroupe(e);
	}
	
	@Test
	public void testTrierAlpha() throws GroupeException {
		Identite i = new Identite("5678", "Zayer", "Kircher");
		Resultat res = new Resultat(this.formation);
		Etudiant e = new Etudiant(i, this.formation, res);
		this.grp.ajouterEtudiant(e);
		this.grp.ajouterEtudiant(etu);
		
		this.grp.trierAlpha();

		List<Etudiant> l = new ArrayList<>();
		for(int ii = 0; ii<grp.getEtudiants().size();ii++) {
			l.add(ii, grp.getEtudiants().get(ii));
		}
		assertEquals("L'odre devrait etre M T", "[Th�o, Zayer]", l.toString());
	}

}
